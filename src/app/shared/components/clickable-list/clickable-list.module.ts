import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickableListComponent } from './clickable-list.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [ClickableListComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ClickableListComponent]
})
export class ClickableListModule { }
