import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SelectableItem } from './clickable-list.model';

@Component({
  selector: 'app-clickable-list',
  templateUrl: './clickable-list.component.html',
  styleUrls: ['./clickable-list.component.scss'],
})
export class ClickableListComponent implements OnInit {

  @Input() items: SelectableItem[];
  @Input() loadingItemId: string;

  @Output() itemSelection = new EventEmitter<SelectableItem>();

  constructor() { }

  ngOnInit() {}

  selectItem(item: SelectableItem): void {
    this.itemSelection.emit(item);
  }

}
