import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { BluetoothSerial } from '@awesome-cordova-plugins/bluetooth-serial/ngx';
import { Platform } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DeviceConnectedGuard implements CanActivate {
  constructor(
    private router: Router,
    private bluetoothSerial: BluetoothSerial,
    private pl: Platform
    ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Promise<boolean | UrlTree>
     {
    try {
      await this.pl.ready();

      const isAlreadyConnectedToABtDevice = await this.bluetoothSerial.isConnected();

      console.log('isAlreadyConnectedToABtDevice', isAlreadyConnectedToABtDevice  === 'OK');

      return isAlreadyConnectedToABtDevice === 'OK';

    } catch (error) {
      this.router.navigate(['home/devices']);
      console.log('ERROR', error);
      return false;
    }
  }
}
