import { TestBed } from '@angular/core/testing';

import { DeviceConnectedGuard } from './device-connected.guard';

describe('DeviceConnectedGuard', () => {
  let guard: DeviceConnectedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(DeviceConnectedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
