export interface BtDevice {
    class: string;
    id: string;
    name: string;
    address: string;
}
