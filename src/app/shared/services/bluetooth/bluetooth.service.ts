import { Injectable } from '@angular/core';
import { BluetoothSerial } from '@awesome-cordova-plugins/bluetooth-serial/ngx';
import { from, interval, Observable, of, pipe } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { BtDevice } from './bluetooth.model';

@Injectable({
  providedIn: 'root',
})
export class BluetoothService {
  constructor(
    private bluetoothSerial: BluetoothSerial
  ) {}

  /**
   * Check if the device is already connected with another device
   *
   * @returns boolean
   */
  checkIfBtIsConnected$(): Observable<boolean> {
    return interval(500).pipe(
      switchMap(() =>
        from(
          this.bluetoothSerial
            .isConnected()
            .then(() => true)
            .catch(() => false)
        )
      )
    );
  }

  /**
   * Check if the BT of the device is enabled
   *
   * @returns boolean
   */
  checkIfBtIsEnabled$(): Observable<boolean> {
    return interval(500).pipe(
      switchMap(() =>
        from(
          this.bluetoothSerial
            .isEnabled()
            .then(() => true)
            .catch(() => false)
        )
      )
    );
  }

  /**
   * List of already paired devices
   *
   * @returns list of devices info
   */
  listBondedDevices$(): Observable<BtDevice[]> {
    return from(this.bluetoothSerial.list()).pipe(
      tap(data => console.log('Paired devices check', data))
    );
  }

  /**
   * List of unpaired devices
   *
   * @returns list of devices info
   */
  listUnpairedDevices$(): Observable<any> {
    console.log('List unpaired devices metoda');
    const listener = this.bluetoothSerial
      .setDeviceDiscoveredListener()
      .subscribe((val) => console.log('LALALA'));
    const discover = this.bluetoothSerial
      .discoverUnpaired()
      .then((found) => console.log('Fount devices', found))
      .catch((error) => console.log('Error finding unpaired devices', error));

    return of();
  }

  /**
   * Send data to connected bt device
   *
   * @param data
   * @returns
   */
  sendData(data: string): Observable<any> {
    return from(this.bluetoothSerial.write(data));
  }

  /**
   * Subscribe to received data from connected bt device
   *
   * @returns
   */
  receiveData(): Observable<any> {
    return this.bluetoothSerial.subscribeRawData();
  }

  /**
   *Start bt connection to a specific device
   *
   * @param macAdress
   * @returns
   */
  startBtConnection(macAdress: string): Observable<void> {
    return this.bluetoothSerial.connect(macAdress).pipe(
      tap(connectionStatus => console.log('Connectin status', connectionStatus)),
    );
  }

  read(): any {
    return this.bluetoothSerial.read();
  }
}
