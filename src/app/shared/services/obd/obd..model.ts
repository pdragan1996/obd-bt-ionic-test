export enum OBDParameters {
    speed = 'speed',
    rpm = 'rpm',
    fuel = 'fuel',
    odo = 'odo',
    hours = 'hours'
}
