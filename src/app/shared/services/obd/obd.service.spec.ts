import { TestBed } from '@angular/core/testing';

import { ObdService } from './obd.service';

describe('ObdService', () => {
  let service: ObdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
