import { Injectable } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { BtDevice } from '../bluetooth/bluetooth.model';
import { BluetoothService } from '../bluetooth/bluetooth.service';

@Injectable({
  providedIn: 'root'
})
export class ObdService {

  speedCmd = '010D\r';
  rmpCmd = '010C\r';
  fuelCmd = '012F\r';
  hours = '017F\r';
  odo = '01A6\r'

  updateFrequency = 500;

  constructor(
    private btProvider: BluetoothService
  ) { }

  /**
   * Check device connection status
   *
   * @returns
   */
  checkBtConnectionStatus(): Observable<boolean> {
    return this.btProvider.checkIfBtIsConnected$();
  }

  /**
   * Check if bt is enabled
   *
   * @returns
   */
  checkIfBtIsEnabled(): Observable<boolean> {
    return this.btProvider.checkIfBtIsEnabled$();
  }

  /**
   * Get list of paired devices
   *
   * @returns
   */
  getPairedDevices(): Observable<BtDevice[]> {
    return this.btProvider.listBondedDevices$();
  }

  /**
   * Start the connection with the OBD2 device
   *
   * @param macAdress
   * @returns
   */
  startBtConnection(macAdress: string): Observable<any> {
    return this.btProvider.startBtConnection(macAdress).pipe(
      tap(() => this.sendInitialCommands())
    );
  }


  /**
   * Send commands for the communication setup with the obdii device
   */
  sendInitialCommands(): void {
    this.btProvider.sendData('ATZ\r');
    //Turns off extra line feed and carriage return
    this.btProvider.sendData('ATL0\r');
    //This disables spaces in in output, which is faster!
    this.btProvider.sendData('ATS0\r');
    //Turns off headers and checksum to be sent.
    this.btProvider.sendData('ATH0\r');
    //Turns off echo.
    this.btProvider.sendData('ATE0\r');
    //Turn adaptive timing to 2. This is an aggressive learn curve for adjusting the timeout. Will make huge difference on slow systems.
    this.btProvider.sendData('ATAT2\r');
    //Automatic protocol detection - http://www.obdtester.com/elm-usb-commands
    this.btProvider.sendData('ATSP0\r');

    this.btProvider.sendData('03\r');


    // this.requestFuelData().subscribe();
    // this.requestRpmData().subscribe();
    // this.requestSpeedData().subscribe();2^2
  }

  /**
   * Get info about the speed from obdii
   *
   * @returns
   */
  requestSpeedData(): Observable<any> {
    return interval(this.updateFrequency).pipe(
      mergeMap(() => this.btProvider.sendData(this.speedCmd))

    );
  }

  /**
   * Get info about the rpm from obdii
   *
   * @returns
   */
  requestRpmData(): Observable<any> {
    return interval(this.updateFrequency).pipe(
      mergeMap(() => this.btProvider.sendData(this.rmpCmd))
    );
  }

  /**
   * Get info about the fuel from obdii
   *
   * @returns
   */
  requestFuelData(): Observable<any> {
    return interval(this.updateFrequency).pipe(
      mergeMap(() => this.btProvider.sendData(this.fuelCmd))
    );
  }

  requestOdo(): Observable<any> {
    return interval(this.updateFrequency).pipe(
      mergeMap(() => this.btProvider.sendData(this.odo))
    );
  }

  requestHours(): Observable<any> {
    return interval(this.updateFrequency).pipe(
      mergeMap(() => this.btProvider.sendData(this.hours))
    );
  }

  checkOBDResponse(): Observable<any> {
    return this.btProvider.receiveData();
  }

  read(): any {
    return this.btProvider.read();
  }

  convertSpeed(data: string): number {
    console.log('SPEED WITHOUT CONVERTING TO ARRAY', data);

    if (!data) {
      return;
    }

    const bytes = this.responseParser(data);

    if (!bytes.length) { return 0; }

    console.log('DATA IN ARRAY FORMAT', bytes);

    const byteA = bytes[2];

    console.log('SPEED AFTER JSON STRINGIFY', byteA);

    return Number(`0x${byteA}`);
  }

  convertRpm(data: string): number {
    console.log('RPM WITHUT WITHOUT CONVERTING TO ARRAY', data);

    if (!data || data === ' ' || data === undefined) {
      return;
    }

    const bytes = this.responseParser(data);

    console.log('What we received from the parser method? ', bytes);

    if (!bytes || !bytes.length) {
      console.log('Bytes is inexistent od witouth lenght');
      return 0; }

    console.log('DATA IN ARRAY FORMAT', bytes);

    const byteA = bytes[2];
    const byteB = bytes[3];

    console.log('RPM BYTE A', byteA);
    console.log('RPM BYTE B', byteB);

    console.log('Byte A converted to number', Number(`0x${byteA}`));
    console.log('Byte B converted to number', Number(`0x${byteB}`));


    const rpm = Number(`0x${byteA}`) * 64 + Number(`0x${byteB}`) /256;


    console.log('Calculated rpm value', rpm);
    return rpm;
  }

  convertFuel(data: string): number {
    console.log('WITHUT WITHOUT CONVERTING TO ARRAY', data);

    if (!data) {
      return;
    }

    const bytes = this.responseParser(data);

    if (!bytes.length) { return 0; }

    console.log('DATA IN ARRAY FORMAT', bytes);

    const byteA = bytes[2];

    console.log('FUEL BYTE A', byteA);

    const fuel = 25 * Number(`0x${byteA}`) / 23;

    return fuel;
  }

  convertOdometar(data: string): number {
    console.log('odometar', data);

    if (!data) {
      return;
    }

    const bytes = this.responseParser(data);

    if (!bytes.length) { return 0; }

    console.log('DATA IN ARRAY FORMAT', bytes);

    const byteA = bytes[2];
    const byteB = bytes[3];
    const byteC = bytes[4];
    const byteD = bytes[5];

    console.log('SPEED AFTER JSON STRINGIFY', byteA, byteB, byteC, byteD);

    return ((Number(`0x${byteA}`) * 2**24) + (Number(`0x${byteB}`) * 2**16) + (Number(`0x${byteC}`) * 2**8) + Number(`0x${byteD}`)) / 10;
  }

  convertHours(data: string): number {
    console.log('hours data', data);

    if (!data) {
      return;
    }

    const bytes = this.responseParser(data);

    if (!bytes.length) { return 0; }

    console.log('DATA IN ARRAY FORMAT for hours', bytes);

    const byteA = bytes[2];


    console.log('hours AFTER JSON STRINGIFY', byteA);

    return Number(`0x${byteA}`);
  }

  responseParser(hexString: string): string[] {
    const responsesToNotParse = [
      'NO DATA',
      'OK',
      '?',
      'UNABLE TO CONNECT',
      'SEARCHING...'
    ];

    const responseBytes = [];

    if (responsesToNotParse.includes(hexString)) {
      return [];
    }

    hexString = hexString.replace(/ /g, '').replace(/(\r\n|\n|\r)/gm,'').replace('>', '');

    for (let byteCounter = 0; byteCounter < hexString.length; byteCounter += 2) {
      // console.log('Byte counter', byteCounter, 'substr from ', byteCounter, 'to ', byteCounter + 2);
      responseBytes.push(hexString.substring(byteCounter, byteCounter + 2));
    }

  console.log('Check response bytes in the parser method', responseBytes);

  console.log('Check the first item of the responseBytes array: ', responseBytes[0]);

  console.log('Is the first item of the array 41 string ? ', responseBytes[0] === '41');

  // return the bytes only if it's a response from the OBD
  if (responseBytes[0] === '41') {
    return responseBytes;
  }




    return [];
  }
}
