import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceConnectedGuard } from 'src/app/shared/guards/device-connected.guard';
import { CalibrationOptionsComponent } from './calibration-options/calibration-options.component';
import { DeviceChooserComponent } from './device-chooser/device-chooser.component';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: 'calibration',
    component: CalibrationOptionsComponent,
    canActivate : [DeviceConnectedGuard]
  },
  {
    path: 'devices',
    component: DeviceChooserComponent,
  },
  {
    path: '',
    redirectTo: 'calibration',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
