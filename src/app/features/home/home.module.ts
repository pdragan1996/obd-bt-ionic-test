import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { DeviceChooserComponent } from './device-chooser/device-chooser.component';
import { CalibrationOptionsComponent } from './calibration-options/calibration-options.component';
import { ClickableListModule } from 'src/app/shared/components/clickable-list/clickable-list.module';
import { CalibrationCheckerComponent } from './calibration-checker/calibration-checker.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClickableListModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, DeviceChooserComponent, CalibrationOptionsComponent, CalibrationCheckerComponent]
})
export class HomePageModule {}
