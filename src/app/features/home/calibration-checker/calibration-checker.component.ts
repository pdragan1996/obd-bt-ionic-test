import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OBDParameters } from 'src/app/shared/services/obd/obd..model';
import { ObdService } from 'src/app/shared/services/obd/obd.service';

@Component({
  selector: 'app-calibration-checker',
  templateUrl: './calibration-checker.component.html',
  styleUrls: ['./calibration-checker.component.scss'],
})
export class CalibrationCheckerComponent implements OnInit, OnDestroy {

  @Input() parameter: string;

  unsubscribe$ = new Subject();

  currentValue: number;

  constructor(
    private changeRef: ChangeDetectorRef,
    private obdService: ObdService) { }

  ngOnInit() {
    this.startFollowingData();
  }

  startFollowingData(): void {
    let requestDataMethod;
    let converterMethod;

    switch (this.parameter) {
      case OBDParameters.speed:
        requestDataMethod = this.obdService.requestSpeedData();
        converterMethod = this.obdService.convertSpeed;
        break;
      case OBDParameters.rpm:
        requestDataMethod = this.obdService.requestRpmData();
        converterMethod = this.obdService.convertRpm;
        break;
      case OBDParameters.fuel:
        requestDataMethod = this.obdService.requestFuelData();
        converterMethod = this.obdService.convertFuel;
        break;
      case OBDParameters.odo:
        requestDataMethod = this.obdService.requestOdo();
        converterMethod = this.obdService.convertOdometar;
        break;
      case OBDParameters.hours:
        requestDataMethod = this.obdService.requestHours();
        converterMethod = this.obdService.convertHours;
        break;
    }

    requestDataMethod
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe();

    this.obdService.checkOBDResponse()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.obdService.read().then(data => {
        console.log('Date before converting...', data);
        const value = converterMethod.call(this.obdService, data);

        console.log('Converter value inside the componente', value);

        if (!value) {return;}

        console.log('Formatted value: ', value);

        this.currentValue = value;

        this.changeRef.detectChanges();

        console.log('This current value', this.currentValue);
      }));


  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
