import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectableItem } from 'src/app/shared/components/clickable-list/clickable-list.model';
import { BtDevice } from 'src/app/shared/services/bluetooth/bluetooth.model';
import { ObdService } from 'src/app/shared/services/obd/obd.service';

@Component({
  selector: 'app-device-chooser',
  templateUrl: './device-chooser.component.html',
  styleUrls: ['./device-chooser.component.scss'],
})
export class DeviceChooserComponent implements OnInit {

  loadingConnectionForDeviceId: string;

  exampleItems: SelectableItem[] = [
    {
      id: '1',
      name: 'Device 1'
    },
    {
      id: '2',
      name: 'Device 2'
    },
    {
      id: '3',
      name: 'CaDevice 3'
    },
    {
      id: '4',
      name: 'Device 4'
    },
    {
      id: '5',
      name: 'Device 5'
    }
  ];

  devices: Observable<SelectableItem[]>;

  constructor(
    private router: Router,
    private obdService: ObdService) { }

  ngOnInit() {
    this.devices = this.obdService.getPairedDevices().pipe(
      // eslint-disable-next-line arrow-body-style
      map((devices: BtDevice[]) => devices.map(device => { return { id: device.address, name: device.name };}))
    );
  }

  selectDevice(device: SelectableItem) {
    console.log('Selected device', device);
    this.loadingConnectionForDeviceId = device.id;

    this.obdService.startBtConnection(device.id).subscribe(() => {
      console.log('CONNECTION STARTED');
      delete this.loadingConnectionForDeviceId;
      this.router.navigate(['home/calibration']);
    });

    setTimeout(() => {
    }, 5000);
  }

}
