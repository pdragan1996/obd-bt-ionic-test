import { Component, OnInit } from '@angular/core';
import { SelectableItem } from 'src/app/shared/components/clickable-list/clickable-list.model';
import { OBDParameters } from 'src/app/shared/services/obd/obd..model';

@Component({
  selector: 'app-calibration-options',
  templateUrl: './calibration-options.component.html',
  styleUrls: ['./calibration-options.component.scss'],
})
export class CalibrationOptionsComponent implements OnInit {

  isModalOpen = false;
  selectedParameter: string;

  exampleItems: SelectableItem[] = [
    {
      id: '1',
      name: OBDParameters.speed
    },
    {
      id: '2',
      name: OBDParameters.rpm
    },
    {
      id: '3',
      name: OBDParameters.fuel
    },
    {
      id: '4',
      name: OBDParameters.odo
    },
    {
      id: '5',
      name: OBDParameters.hours
    }
  ];

  constructor() { }

  ngOnInit() {}

  selectCalibrationOption(item: SelectableItem): void {
    this.isModalOpen = true;
    this.selectedParameter = item.name;
  }
}
