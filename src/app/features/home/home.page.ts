import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { from, Observable, pipe } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';
import { BtDevice } from '../../shared/services/bluetooth/bluetooth.model';
import { AndroidPermissionResponse, AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { ObdService } from '../../shared/services/obd/obd.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  isBtConnected$: Observable<boolean>;
  isBtEnabled$: Observable<boolean>;
  isPlatformReady = false;
  list$: Observable<BtDevice[]>;
  listOfUnpaired: any;
  loadingUnpaired = false;

  receivedData$: Observable<any>;

  debug: any;

  constructor(
    private pl: Platform,
    private permissions: AndroidPermissions,
    private obdService: ObdService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.getPlatformStauts$()
      .pipe(
        tap(status => this.isPlatformReady = status === 'cordova'),
        filter(status => status === 'cordova'),
        tap(() => this.permissions.checkPermission(this.permissions.PERMISSION.ACCESS_FINE_LOCATION)
          .then(() =>
            (result: AndroidPermissionResponse) => console.log('Has permission?',result.hasPermission),
            err => this.permissions.requestPermission(this.permissions.PERMISSION.ACCESS_FINE_LOCATION)
          )
        ),
        tap(() => this.isBtConnected$ = this.obdService.checkBtConnectionStatus()),
        tap(() => this.list$ = this.obdService.getPairedDevices()),
        tap(() => this.isBtEnabled$ = this.obdService.checkIfBtIsEnabled())
      ).subscribe();

      // this.receivedData$ = this.obdService.checkOBDResponse();
  }

  getPlatformStauts$(): Observable<string> {
    return from(this.pl.ready());
  }

  selectDevice(device: BtDevice): void {
    console.log('Selected device', device);

    this.obdService.startBtConnection(device.address).subscribe({
      next: (data)  => {
        console.log('Inside startBtConnection subscribe, ', data);
        // this.receivedData$ = this.obdService.checkOBDResponse();
        this.obdService.checkOBDResponse().subscribe((dt) => {
          console.log('Inside startBtConnection subscribe 2, ', dt);
          this.obdService.read().then((dd) => {
            console.log('Inside startBtConnection subscribe 3, ', dd);
            this.onDataReceive(dd);
            this.cdr.detectChanges(); // either here
           });
        });
      }
    });
  }

  onDataReceive(dd) {
    this.debug += '\n' + JSON.stringify(dd);
    this.cdr.detectChanges(); // or here
  }

}
